import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) { 
  return new Promise((resolve) => {      
    const keyCombination = new Set();
    const playersInterface = {
      criticalHitTime: Date.now() - 10000,
      onBlock: false,
      gethealthIndicator () {
        return this.health * 100 / this.startHelth;
      },
      getCriticalHit () {
        return this.attack * 2;
      },
      setHealth(damage) {
        this.health = this.health - damage < 0 ? 0 : this.health - damage;
      },
      setBlock (block) {
        this.onBlock = block;
      },
      setCriticalHitTime () {
        this.criticalHitTime = Date.now();
      },
      setHelhtBar () {
        this.playerHelthBar.style.width = `${this.gethealthIndicator()}%`;
      },
    } 
    const playerOne = {
      ...firstFighter,
      ...playersInterface,
      startHelth: firstFighter.health,      
      playerHelthBar: document.getElementById('left-fighter-indicator'),

    }
    const playerTwo = {
      ...secondFighter,
      ...playersInterface,
      startHelth: secondFighter.health,
      playerHelthBar: document.getElementById('right-fighter-indicator'),
      
    }

    const findWiner = (attacker, defender) => {
      if(defender.health === 0){
        resolve(attacker)
      }
    }

    const makeHit = (attacker, defender) => {
      if(!defender.onBlock && !attacker.onBlock){
        defender.setHealth(getDamage(attacker,defender));
        defender.setHelhtBar();
        findWiner(attacker, defender);
      }
    }

    const criticalHit = (attacker, defender) => {
      const time = Date.now();
      
      if(time - attacker.criticalHitTime > 10000){
        defender.setHealth(attacker.getCriticalHit());
        defender.setHelhtBar();
        attacker.setCriticalHitTime(Date.now());
        findWiner(attacker, defender);
      }
    }

    const keyUp = e => {
      keyCombination.clear();
      switch (e.code) {
        case controls.PlayerOneBlock:
          playerOne.setBlock(false);
          break;
          
        case controls.PlayerTwoBlock:
          playerTwo.setBlock(false);
          break;        
      }
    }

    const keyDown = e => {      
      switch (e.code) {
        case controls.PlayerOneAttack:
          if(!e.repeat){
            makeHit(playerOne,playerTwo);
          }                      
          break;

        case controls.PlayerOneBlock:
          playerOne.setBlock(true);
          break;

        case controls.PlayerTwoAttack:
          if(!e.repeat){
            makeHit(playerTwo,playerOne);
          }        
          break;            

        case controls.PlayerTwoBlock:
          playerTwo.setBlock(true);
          break;
                  
        default:
          keyCombination.add(e.code);

          if(keyCombination.size > 2){
            const playerOneCrit = controls.PlayerOneCriticalHitCombination.every(el => keyCombination.has(el));
            const playerTwoCrit = controls.PlayerTwoCriticalHitCombination.every(el => keyCombination.has(el));

            if(playerOneCrit){
              criticalHit(playerOne,playerTwo);
            }
            if(playerTwoCrit){
              criticalHit(playerTwo,playerOne);
            }             
          }
          break;
      }  
    }

    document.addEventListener('keydown', keyDown);
    document.addEventListener('keyup', keyUp);
  });
}

export function getDamage(attacker, defender) {
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  
  return blockPower > hitPower ? 0 : hitPower - blockPower;
}

export function getHitPower(fighter) {
  const { attack } = fighter;
  const criticalHitPower = Math.random() + 1;

  return attack * criticalHitPower;
}

export function getBlockPower(fighter) {
  const { defense  } = fighter;
  const blockPower = Math.random() + 1;

  return defense * blockPower;
}
