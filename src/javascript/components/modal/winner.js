import {showModal} from './modal';

export function showWinnerModal(fighter) {
  const winner = {
    title: 'You rock!',
    bodyElement: fighter.name,
    onClose: () => {window.location.reload()}
  }
  
  showModal(winner);
}
